Dev Tunnel
=============

# Description

P2p tunnels for developers

# Usage

To install Dev Tunnel from npm, run:

```
$ npm install -g dev-tunnel
```

```node ./bin/dev-tunnel --help```

# License

Copyright (c) 2015 Hermano Cabral

[MIT License](http://en.wikipedia.org/wiki/MIT_License)

# Acknowledgments

Foo.
