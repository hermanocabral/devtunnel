/* status commander component
 * To use add require('../cmds/status.js')(program) to your commander.js based node executable before program.parse
 */
'use strict';

module.exports = function(program) {

	program
		.command('status')
		.version('0.0.0')
		.description('Gets the server status')
		.action(function(/* Args here */){
			// Your code goes here
		});
	
};