/* connect commander component
 * To use add require('../cmds/connect.js')(program) to your commander.js based node executable before program.parse
 */
'use strict';

module.exports = function(program) {

	program
		.command('connect')
		.version('0.0.0')
		.description('Connects to something')
		.action(function(/* Args here */){
			// Your code goes here
		});
	
};